#ifndef RNS_H
#define RNS_H

#include <vector>
#include <ostream>

class RNS
{
public:		//typedefs
	/**
	 * @brief rnsWordData single RNS word cell.
	 * Can be changed, but should remain unsigned
	 */
	typedef unsigned int rnsWordData;

	/**
	 * @brief rnsWord type for single RNS word
	 *
	 * THIS IS NOT INITIATED TO PROPER SIZE!
	 * Use emptyWord() instead!
	 */
	typedef std::vector<rnsWordData> rnsWord;

	/**
	 * @brief The rnsSignedWord struct type for single RNS signed word
	 * @details Sign is implemented in IEEE754-like style, as additional bit
	 */
	struct rnsSignedWord
	{
		bool isNegative;
		rnsWord word;

		rnsSignedWord(unsigned int wordLen=0);
		rnsSignedWord & operator=(const RNS::rnsSignedWord& other);
		rnsSignedWord & operator=(const RNS::rnsWord &other);
	};

protected:	//RNS vars
	rnsWord M;
	rnsWord lifo;

	unsigned int lenFract;	//length of fraction part
	unsigned int lenInt;	//length of integer/whole part
	unsigned int lenExt;

	unsigned int Rf;		//fraction accuracy
	unsigned int maxInt;	//integer accuracy
	unsigned int maxMach;	//machine version (internal) accuracy

public:		//user functions
	RNS(unsigned int intpart,unsigned int fractpart);//init
	void multiply(rnsWord &a, rnsWord &b, rnsWord &result);
	void multiply(rnsSignedWord &a, rnsSignedWord &b, rnsSignedWord &result);

	void dec2extended(float dec, rnsWord& result);
	void dec2extended(float dec, rnsSignedWord& result);
	rnsWord emptyWord() const;
	rnsSignedWord emptySignedWord() const;
	unsigned int getWordLen() const;

protected:	//helpers
	rnsWordData modDiv(const rnsWordData &a, const rnsWordData &b, const rnsWordData &m) const;

	void primes(unsigned int count, rnsWord &result);

	/*
	 *	Multiplication modules based on
	 *	US 20130311532Al Figure 15A
	 */
	void getIntermediate(rnsWord &a, rnsWord &b, rnsWord &result);
	void inter2mixed(rnsWord &src, rnsWord &result);
	void mixed2rns(rnsWord &src, rnsWord &result);

public:		//tests
	static bool test();
	static bool test2();
	static bool test3();
};

std::ostream& operator<<(std::ostream& os, const RNS::rnsWord& word);
std::ostream& operator<<(std::ostream& os, const RNS::rnsSignedWord& word);
bool operator==(const RNS::rnsSignedWord &rw, const RNS::rnsSignedWord &lw);
bool operator!=(const RNS::rnsSignedWord &rw, const RNS::rnsSignedWord &lw);

#endif // RNS_H
