#include <iostream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <algorithm>

#include "rnsMult.h"

/**
 * @brief RNS::RNS prepare algorithm to work in specified configuration
 * @param intpart ammount of modules responsible for holding integer/whole part
 * @param fractpart ammount of modules responsible for holding fraction part
 */
RNS::RNS(unsigned int intpart, unsigned int fractpart)
{
	lenFract = fractpart;
	lenInt = intpart;
	lenExt = lenFract+lenInt;

	lifo.resize(lenExt*2-1);
	M.resize(getWordLen());
	primes(lenExt,M);

	Rf = 1;
	maxInt=1;
	for(unsigned int i=0;i<lenFract;i++)
		Rf *=M[i];

	for(unsigned int i=lenFract;i<lenExt;i++)
		maxInt *=M[i];

	maxMach=maxInt*Rf;
}

/**
 * @brief RNS::multiply multiply two RNS fixed-point words
 * @param a first RNS word
 * @param b second RNS word
 * @param result RNS word which will store the result of multiplication
 */
void RNS::multiply(rnsWord &a, rnsWord &b, rnsWord &result)
{
	//TODO: store intermediate in local array?
	getIntermediate(a,b,result);
	inter2mixed(result,lifo);
	mixed2rns(lifo,result);
}

/**
 * @brief RNS::multiply multiply two signed RNS fixed-point words
 * @param a first RNS signed word
 * @param b second RNS signed word
 * @param result RNS word which will store the result of multiplication
 */
void RNS::multiply(RNS::rnsSignedWord &a, RNS::rnsSignedWord &b, RNS::rnsSignedWord &result)
{
	multiply(a.word,b.word,result.word);
	result.isNegative=a.isNegative^b.isNegative;
	if(!result.isNegative)
		return;
	//force zero to always be positive
	for (unsigned int i = 0; i < result.word.size(); ++i)
		if(result.word[i])
			return;
	result.isNegative=false;
}

/**
 * @brief RNS::dec2extended convert decimal number to RNS unsigned format
 * @param dec decimal number
 * @param result RNS word which will store decimal represented as RNS value
 */
void RNS::dec2extended(float dec, rnsWord &result)
{
	unsigned int temp;
	temp = ((unsigned int)std::floor(std::abs(dec)*Rf)) %(maxMach);

	if(dec>maxInt)
		std::cout << "Overflow! Not enough modules to convert decimal value to fixed point RNS representation." << std::endl;

	if(dec<0)
		std::cout << "Underflow! System is not designed to handle negative values" << std::endl;

	unsigned int gcd = std::__gcd(temp,Rf);

	std::cout << "Info: " << std::setprecision(4) << dec
			  << " will be saved as: " << temp/gcd << "/" << Rf/gcd << std::endl;

	//Can be run in parallel
	for(unsigned int i=0;i<lenExt;i++)
		result[i]=temp%M[i];
}

/**
 * @brief RNS::dec2extended convert decimal number to RNS signed format
 * @param dec decimal number
 * @param result RNS word which will store decimal represented as RNS value
 */
void RNS::dec2extended(float dec, RNS::rnsSignedWord &result)
{
	unsigned int temp;
	result.isNegative=dec<0;
	dec = std::abs(dec);
	temp = ((unsigned int)std::floor(dec*Rf)) %(maxMach);

	if(dec>maxInt)
		std::cout << "Overflow! Not enough modules to convert decimal value to fixed point RNS representation." << std::endl;

	unsigned int gcd = std::__gcd(temp,Rf);

	std::cout << "Info: " << (result.isNegative?"-":"+") << std::setprecision(4) << dec
			  << " will be saved as: " << (result.isNegative?"-":"+") << temp/gcd << "/" << Rf/gcd << std::endl;

	//Can be run in parallel
	for(unsigned int i=0;i<lenExt;i++)
		result.word[i]=temp%M[i];
}

/**
 * @brief RNS::emptyWord initialise rnsWord to proper size
 * @return rnsWord empty RNS word
 */
RNS::rnsWord RNS::emptyWord() const
{
	return rnsWord(getWordLen());
}
/**
 * @brief RNS::emptySignedWord initialise rnsWord to proper size and set value as positive
 * @return empty RNS signed word
 */
RNS::rnsSignedWord RNS::emptySignedWord() const
{
	return rnsSignedWord(getWordLen());
}

/**
 * @brief RNS::getWordLen returns ammount of primes used to represent RNS word
 * @return RNS word length
 */
unsigned int RNS::getWordLen() const
{
	return lenExt;
};

/**
 * @brief RNS::modDiv modulo for (a/b)%m equation.
 * @param a modulo dividend' dividend
 * @param b modulo dividend' divisor
 * @param m modulo divisor
 * @return integer
 * @throw std::invalid_argument thrown when algorithm failed to generate result - should never happen.
 */
RNS::rnsWordData RNS::modDiv(const rnsWordData &a, const rnsWordData &b, const rnsWordData &m) const
{
	//If a/b division is fractionless, we can skip extended modulo process
	if(a%b==0)
		return((a/b)%m);

	//Could be run in parallel
	//if we check that only one proper result is generated
	for(rnsWordData i=0;i<m;i++)
		if((b*i)%m==a)
			return(i);

	std::stringstream ss;
	ss << "failed to generate result to modulo (" << a << "/" << b << ")%" << m;
	throw std::invalid_argument(ss.str());
}

/**
 * @brief RNS::primes generate modules used for RNS word
 * @param count ammount of primes
 * @param result RNS word which will store primes
 * @throw std::invalid_argument thrown when unsigned int is too small to hold count primes
 */
void RNS::primes(unsigned int count, rnsWord &result)
{
	//naive implementation

	auto isPrime = [](int n)
	{
		for(int i = 2; i <= n/2; i++)
			if (n%i == 0)
				return false;
		return true;
	};

	unsigned int primecnt=0;

	for (unsigned int i = 2; i < UINT_MAX; ++i)
	{
		if(!isPrime(i))
			continue;

		result[primecnt++]=i;
		if(primecnt==count)
			return;

	}

	throw std::invalid_argument("unsigned int is too small to hold high enough prime!");
}


/**
 * @brief RNS::getIntermediate 1510 module of US 20130311532Al Figure 15A
 * @details initially multiplies RNS words like they weren't fixed point
 * @param a first RNS word
 * @param b second RNS word
 * @param result RNS word which will store intermediate value
 */
void RNS::getIntermediate(rnsWord &a, rnsWord &b, rnsWord &result)
{
	//Can be run in parallel
	for(unsigned int i=0;i<lenExt;i++)
		result[i]=(a[i]*b[i])%M[i];
}


/**
 * @brief RNS::inter2mixed 1520 module of US 20130311532Al Figure 15A
 * @details converts RNS word to mixed radix LIFO representation
 * @param src RNS word
 * @param result mixed radix LIFO
 */
void RNS::inter2mixed(rnsWord &src, rnsWord &result)
{
	rnsWord &lifo = result;
	unsigned int li = 0;

	for(unsigned int i=0;i<lenExt;i++)
	{
		lifo[li]=src[i];
		li++;

		rnsWordData temp;
		temp=src[i];
		if(temp!=0)
			//Can be run in parallel
			for(unsigned int j=i;j<lenExt;j++)
				src[j]=(src[j]-temp+M[j])%M[j];


		if(i==lenExt-1)
			break;

		lifo[li]=M[i];
		li++;

		//Can be run in parallel
		for(unsigned int j=i;j<lenExt;j++)
			src[j] = modDiv(src[j], M[i], M[j]);
	};
}

/**
 * @brief RNS::mixed2rns 1530 module of US 20130311532Al Figure 15A
 * @details converts mixed radix LIFO to RNS value. Skips last lenFract*2 values in LIFO
 * @param src mixed radix LIFO with intermediate value
 * @param result RNS word with multiplication result
 */
void RNS::mixed2rns(rnsWord &src, rnsWord &result)
{
	rnsWord &lifo = src;

	//TODO:check values and use while
	for(unsigned int i=lenExt*2-2;;)
	{

		//Can be run in parallel
		for(unsigned int j=0;j<lenExt;j++)
			result[j]=(result[j]+lifo[i])%M[j];

		if(i==lenFract*2)
			break;
		i--;

		//Can be run in parallel
		for(unsigned int j=0;j<lenExt;j++)
			result[j]=(result[j]*lifo[i])%M[j];
		i--;
	};
}


/*
 *	Tests
 */


/**
 * @brief RNS::test test for data shown on US 20130311532Al figures 7B and 8B
 * @details checks conversion from intermediate to mixed radix LIFO, and from LIFO to rns result
 * @return true if test succeeded
 */
bool RNS::test()
{
	RNS rns(6,0);
	std::cout << "Format 6:0; intermediate as an input" << std::endl;

	//TODO: new and delete
	rnsWord &mixd = rns.lifo;
	rnsWord res(rns.getWordLen());

	//expected values taken from 7B and 8B
	rnsWord eM		= {2, 3, 5, 7, 11, 13};
	rnsWord eMixd	= {1, 2, 2, 3, 0, 5, 0, 7, 5, 11, 9};
	rnsWord eRes	= {1, 2, 0, 5, 10, 5};
	rnsWord inter	= {1,2,0,5,10,5};	//same as eRes


	std::cout << "M: " << rns.M << std::endl;
	if(rns.M!=eM)
	{
		std::cout << "Invalid M! Expected: " << eM << std::endl;
		return false;
	};

	std::cout << "intermediate: " << inter << std::endl;

	rns.inter2mixed(inter,mixd);
	std::cout << "lifo: " << mixd << std::endl;
	if(mixd!=eMixd)
	{
		std::cout << "Invalid mixed radix LIFO! Expected: " << eMixd << std::endl;
		return false;
	};

	rns.mixed2rns(mixd,res);
	std::cout << "truncated result: " << res << std::endl;
	if(res!=eRes)
	{
		std::cout << "Invalid result! Expected: " << eRes << std::endl;
		return false;
	};

	std::cout << "All values match expected ones defined in the patent" << std::endl;
	return true;
}

/**
 * @brief RNS::test2 test for data shown on US 20130311532Al figure 15F
 * @details check values generated at every stage, also checks multiply(a,b,res) validity
 * @return true if test succeeded
 */
bool RNS::test2()
{
	RNS rns(11,7);
	std::cout << "Format 11:7; A = 3+1/7, B = 8+1/5" << std::endl;


	rnsWord A(rns.getWordLen());
	rnsWord B(rns.getWordLen());
	rnsWord inter(rns.getWordLen());
	rnsWord &mixd = rns.lifo;
	rnsWord res(rns.getWordLen());
	rnsWord res2(rns.getWordLen());

	//expected values taken from 15F
	rnsWord eA		= {0, 0, 0, 4, 0, 0, 0, 5, 3, 6, 24, 29, 7, 1, 21, 44, 14, 38};
	rnsWord eB		= {0, 0, 2, 0, 0, 0, 0, 7, 21, 3, 4, 2, 0, 3, 33, 30, 14, 57};
	rnsWord eM		= {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61};
	rnsWord einter	= {0, 0, 0, 0, 0, 0, 0, 16, 17, 18, 3, 21, 0, 3, 35, 48, 19, 31};
	rnsWord eMixd	= {0, 2, 0, 3, 0, 5, 0, 7, 0, 11, 0, 13, 0, 17, 3, 19, 13, 23, 4, 29, 15, 31, 33, 37, 0, 41, 0, 43, 0, 47, 0, 53, 0, 59, 0};
	rnsWord eRes	= {0, 0, 2, 2, 0, 0, 0, 3, 20, 26, 17, 1, 0, 34, 3, 11, 44, 31};


	rns.dec2extended(3+1.0/7,A);
	rns.dec2extended(8+1.0/5,B);
	std::cout << "M: " << rns.M << std::endl;
	std::cout << "A: " << A << std::endl;
	std::cout << "B: " << B << std::endl;

	if(rns.M!=eM)
	{
		std::cout << "Invalid M! Expected: " << eM << std::endl;
		return false;
	};

	if(A!=eA)
	{
		std::cout << "Invalid A! Expected: " << eA << std::endl;
		return false;
	};

	if(B!=eB)
	{
		std::cout << "Invalid B! Expected: " << eB << std::endl;
		return false;
	};

	rns.getIntermediate(A,B,inter);
	std::cout << "intermediate: " << inter << std::endl;
	if(inter!=einter)
	{
		std::cout << "Invalid intermediate! Expected: " << einter << std::endl;
		return false;
	};

	rns.inter2mixed(inter,mixd);
	std::cout << "lifo: " << mixd << std::endl;
	if(mixd!=eMixd)
	{
		std::cout << "Invalid mixed radix LIFO! Expected: " << eMixd << std::endl;
		return false;
	};

	rns.mixed2rns(mixd,res);
	std::cout << "truncated result: " << res << std::endl;
	if(res!=eRes)
	{
		std::cout << "Invalid result! Expected: " << eRes << std::endl;
		return false;
	};

	rns.multiply(A,B,res2);
	if(res2!=eRes)
	{
		std::cout << "Invalid result given by rnsMult wrapper!" << std::endl;
		return false;
	};

	std::cout << "All values match expected ones defined in the patent" << std::endl;
	return true;
}

/**
 * @brief RNS::test3 signed test for data shown on US 20130311532Al figure 15F
 * @details checks validity of operations on signedWord 
 * @return true if test succeeded
 */
bool RNS::test3()
{
	RNS rns(11,7);
	std::cout << "Format 11:7; A = 3+1/7, B = 8+1/5" << std::endl;


	rnsSignedWord A(rns.getWordLen());
	rnsSignedWord B(rns.getWordLen());
	rnsSignedWord res(rns.getWordLen());

	//expected values taken from 15F
	rnsSignedWord eA;
	eA.word = {0, 0, 0, 4, 0, 0, 0, 5, 3, 6, 24, 29, 7, 1, 21, 44, 14, 38};
	rnsSignedWord eB;
	eB.word = {0, 0, 2, 0, 0, 0, 0, 7, 21, 3, 4, 2, 0, 3, 33, 30, 14, 57};
	rnsSignedWord eRes;
	eRes.word = {0, 0, 2, 2, 0, 0, 0, 3, 20, 26, 17, 1, 0, 34, 3, 11, 44, 31};


	auto test = [&](bool negA, bool negB, bool negRes)
	{
		eA.isNegative=negA;
		eB.isNegative=negB;
		eRes.isNegative=negRes;
		rns.dec2extended((negA?-1:1)*(3+1.0/7),A);
		rns.dec2extended((negB?-1:1)*(8+1.0/5),B);

		if(A!=eA)
		{
			std::cout << "A: " << A << std::endl;
			std::cout << "Invalid A! Expected: " << eA << std::endl;
			return false;
		};

		if(B!=eB)
		{
			std::cout << "B: " << B << std::endl;
			std::cout << "Invalid B! Expected: " << eB << std::endl;
			return false;
		};

		rns.multiply(A,B,res);
		if(res!=eRes)
		{
			std::cout << "Invalid result given by " << (negA?"-":"+") << "A * " << (negB?"-":"+") << "B!" << std::endl;
			return false;
		};
		return true;
	};

	if((!test(1,0,1))
	   ||(!test(0,1,1))
	   ||(!test(1,1,0))
	   ||(!test(0,0,0)))
		return false;

	std::cout << "Signed test completed succesfully" << std::endl;

	return true;
}


std::ostream &operator<<(std::ostream &os, const RNS::rnsWord &word)
{

	os << "[";
	for (size_t i = 0; i < word.size(); ++i)
	{
		os<< word[i];
		if(i!=word.size()-1)
			os << ", ";
	}
	os << "]";
	return os;

}

std::ostream &operator<<(std::ostream &os, const RNS::rnsSignedWord& word)
{
	os << (word.isNegative?"-":"+") << word.word;
	return os;
}

RNS::rnsSignedWord::rnsSignedWord(unsigned int wordLen)
{
	isNegative=false;
	word.resize(wordLen);
}

RNS::rnsSignedWord &RNS::rnsSignedWord::operator=(const RNS::rnsSignedWord &other)
{
	isNegative=other.isNegative;
	std::copy(other.word.begin(),other.word.end(),std::back_inserter(word));
	return *this;
}

RNS::rnsSignedWord &RNS::rnsSignedWord::operator=(const RNS::rnsWord &other)
{
	isNegative=false;
	std::copy(other.begin(),other.end(),std::back_inserter(word));
	return *this;
}

bool operator==(const RNS::rnsSignedWord &rw, const RNS::rnsSignedWord &lw)
{
	return (rw.word==lw.word && rw.isNegative==lw.isNegative);
}

bool operator!=(const RNS::rnsSignedWord &rw, const RNS::rnsSignedWord &lw)
{
	return !(rw==lw);
}

