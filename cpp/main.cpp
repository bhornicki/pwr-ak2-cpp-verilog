#include <iostream>
#include "rnsMult.h"

int main()
{
	RNS::test();
	std::cout << std::endl;
	RNS::test2();
	std::cout << std::endl;
	RNS::test3();
	std::cout << std::endl;

	unsigned int integ, fract;

	std::cout << "Insert ammount of whole/integer modules: ";
	std::cin >> integ;
	std::cout << "Insert ammount of fraction modules: ";
	std::cin >> fract;

	std::cout << "Starting RNS multiplier in mode " << integ << ":" << fract << std::endl;
	RNS rns(integ,fract);
	std::cout << std::endl << "To escape press CTRL + C" << std::endl
			  << "Current mode does not support 2/5 format, but it supports 0.4" << std::endl
			  << std::endl;

	float fa, fb;
	RNS::rnsSignedWord ra(rns.getWordLen()),rb(rns.getWordLen()), res(rns.getWordLen());
	while (true)
	{
		std::string str;
		std::cout << "Insert first number: ";
		std::cin >> fa;
		std::cout << "Insert second number: ";
		std::cin >> fb;

		rns.dec2extended(fa,ra);
		rns.dec2extended(fb,rb);
		rns.multiply(ra,rb,res);
		std::cout << "   " << ra << std::endl
				  << " * " << rb << std::endl
				  << " = " << res << std::endl
				  << std::endl;

	}
}
