/*
 *	Global vars
 *	use init(int,fract) to update these
 */

lenFract = 2		\\length of fraction part
lenInt = 2			\\length of integer/whole part
lenExt = lenFract+lenInt
M = primes(lenExt)	\\extended modules vector
Rf = 2*3			\\fraction accuracy
maxInt = 5*7		\\integer accuracy
maxMach = Rf*maxInt	\\machine version (internal) accuracy


/*
 *	Helpers
 */

\\prepare algorithm to work in new configuration
\\ fract - ammount of modules responsible for holding fraction part
\\ int -  ammount of modules responsible for holding integer/whole part
init(int,fract) = 
{
	lenFract = fract;
	lenInt = int;
	lenExt = lenFract+lenInt;
	M = primes(lenExt);
	Rf = 1;
	maxInt=1;
	for(i=1,lenFract,
		Rf *=M[i];
	);
	for(i=lenFract+1, lenExt,
		maxInt *=M[i];
	);
	maxMach=maxInt*Rf;
}

\\convert decimal number to RNS format
dec2extended(dec) = 
{
	my(temp, res);
	temp = floor(abs(dec)*Rf)%(maxMach);
	res=vector(lenExt);
	if(dec>maxInt,
		print("Overflow! Not enough modules to convert decimal value to fixed point RNS representation.");
	);
	if(dec<0,
		print("Underflow! System is not designed to handle negative values");
	);
	print("Info: ", dec, " will be saved as: ", temp/Rf);
	
	\\Can be run in parallel
	for(i=1,lenExt,
		res[i]=temp%M[i];
	);

	return(res);
} 


/*
 *	Multiplication modules based on
 *	US 20130311532Al Figure 15A 
 */

\\1510
getIntermediate(a,b) =
{
	my(res);
	res=vector(lenExt);
	
	\\Can be run in parallel
	for(i=1,lenExt,
		res[i]=(a[i]*b[i])%M[i];
	);
	
	return(res);
}

\\1520
inter2mixed(int) =
{
	my(skip,lifo,d,li);
	lifo = vector(lenExt*2-1);
	d = int;
	li = 1;
	
	for(i=1,lenExt,
		lifo[li]=int[i];
		li++;
		
		my(temp);
		temp=int[i];
		if(temp!=0,
			\\Can be run in parallel
			for(j=i,lenExt,  
				int[j]=(int[j]-temp+M[j])%M[j];
			);
		);
		
		if(i==lenExt, break);
		
		lifo[li]=M[i];
		li++;
		
		\\Can be run in parallel
		for(j=i,lenExt,  
			int[j] = (int[j]/M[i])%M[j];
		);
		
	);
	return(lifo);
}

\\1530
mixed2rns(lifo) =
{
	my(res);
	res=vector(lenExt);
	
	forstep(i=lenExt*2-1,0,-1,
		
		\\Can be run in parallel
		for(j=1,lenExt,  
			res[j]=(res[j]+lifo[i])%M[j];
		);
		
		if(i==lenFract*2+1, break);
		i--;
		
		\\Can be run in parallel
		for(j=1,lenExt,  
			res[j]=(res[j]*lifo[i])%M[j];
		);
	);	
	return(res);
}


/*
 *	Wrapper function
 */

\\multiply two RNS values compatible with modules set previously by init()
rnsMult(A,B) = 
{
 	return(
		mixed2rns(
			inter2mixed(
				getIntermediate(A,B)
			)
		)
	);
};


/*
 *	Tests
 */

\\figure 7B and 8B test
\\check conversion from intermediate to mixed radix lifo
\\and from lifo to rns result
test()=
{
	my(int,mixd,res);
	my(eM,eMixd,eRes);	\\expected values taken from 7B and 8B
	eM		= [2, 3, 5, 7, 11, 13];
	eMixd	= [1, 2, 2, 3, 0, 5, 0, 7, 5, 11, 9];
	eRes	= [1, 2, 0, 5, 10, 5];
	int=[1,2,0,5,10,5];	\\same as eRes
	
	init(6,0);
	print("Format 6 int, 0 fract; intermediate as an input");
	
	print("M: ",M);
	if(	(M!=eM),
		print("Invalid M! Expected: ", eM);
		return(0);
	);
	
	print("intermediate: ",int);
	
	
	mixd=inter2mixed(int);
	print("lifo: ",mixd);
	if(	(mixd!=eMixd),
		print("Invalid mixed radxi LIFO! Expected: ", eMixd);
		return(0);
	);
	
	res=mixed2rns(mixd);
	print("truncated result: ",res);
	if(	(res!=eRes),
		print("Invalid result! Expected: ", eRes);
		return(0);
	);
	
	print("All values match expected ones defined in the patent");
	return(1);
}

\\Figure15F test
\\Check values generated at every stage
\\Also check rnsMult wrapper result
test2() =
{
	my(A,B,int,mixd,res);
	my(eA, eB, eM, eInt,eMixd,eRes);	\\expected values taken from 15F
	eA		= [0, 0, 0, 4, 0, 0, 0, 5, 3, 6, 24, 29, 7, 1, 21, 44, 14, 38];
	eB		= [0, 0, 2, 0, 0, 0, 0, 7, 21, 3, 4, 2, 0, 3, 33, 30, 14, 57];
	eM		= [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61];
	eInt	= [0, 0, 0, 0, 0, 0, 0, 16, 17, 18, 3, 21, 0, 3, 35, 48, 19, 31];
	eMixd	= [0, 2, 0, 3, 0, 5, 0, 7, 0, 11, 0, 13, 0, 17, 3, 19, 13, 23, 4, 29, 15, 31, 33, 37, 0, 41, 0, 43, 0, 47, 0, 53, 0, 59, 0];
	eRes	= [0, 0, 2, 2, 0, 0, 0, 3, 20, 26, 17, 1, 0, 34, 3, 11, 44, 31];
	
	init(11,7);
	print("Format 11 int, 7 fract; A = 3+1/7, B = 8+1/5");
	
	A=dec2extended(3+1/7);
	B=dec2extended(8+1/5);
	print("M: ",M);
	print("A: ",A);
	print("B: ",B);
	
	if(	(M!=eM),
		print("Invalid M! Expected: ", eM);
		return(0);
	);
	
	if(	(A!=eA),
		print("Invalid A! Expected: ", eA);
		return(0);
	);
	
	if(	(B!=eB),
		print("Invalid B! Expected: ", eB);
		return(0);
	);
	
	int=getIntermediate(A,B);
	print("intermediate: ",int);
	if(	(int!=eInt),
		print("Invalid intermediate! Expected: ", eInt);
		return(0);
	);
	
	mixd=inter2mixed(int);
	print("lifo: ",mixd);
	if(	(mixd!=eMixd),
		print("Invalid mixed radxi LIFO! Expected: ", eMixd);
		return(0);
	);
	
	res=mixed2rns(mixd);
	print("truncated result: ",res);
	if(	(res!=eRes),
		print("Invalid result! Expected: ", eRes);
		return(0);
	);
	
	if(	(rnsMult(A,B)!=eRes),
		print("Invalid result given by rnsMult wrapper!");
		return(0);
	);
	
	print("All values match expected ones defined in the patent");
	return(1);
}